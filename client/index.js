var express = require("express");
const https = require("https"),
  fs = require("fs");
  // var mongo = require('mongodb');
// const options = {
//   key: fs.readFileSync("/srv/www/keys/my-site-key.pem"),
//   cert: fs.readFileSync("/srv/www/keys/chain.pem")
// };

const app = express();

app.use(express.static("public"));

app.set("view engine", "ejs");
app.set("views", "./view");

app.listen(4000);

app.get("/home", function(request, response)  {

    response.render("home");
});

app.get("/header", function(request, response)  {

    response.render("header");
});

app.get("/login", function(request, response) {
  response.render("login");
})
