var express = require('express');
var router = express.Router();
var UserAccount = require('./models/UserAccount');
router.get('/find/:id?',function(req,res,next){
    if(req.params.id){
        UserAccount.getUserById(req.params.id,function(err,rows){
            if(err){
                res.json(err);
            }
            else{
                res.json(rows);
            }
        });
    }else{
        UserAccount.getAllUser(function(err,rows){
            if(err){
                res.json(err);
            } else {
                res.json(rows);
            }

        });
    }
});

router.post('/register',function(req,res,next){
    UserAccount.addUser(req.body,function(err,count){
        if(err){
            res.json(err);
        } else{
            res.json(req.body);
        }
    });
});

router.delete('/delete/:id',function(req,res,next){
    UserAccount.deleteUser(req.params.id,function(err,count){
        if(err){
            res.json(err);
        } else{
          res.json(count);
        }
    });
});

router.put('/update/:id',function(req,res,next){
    UserAccount.updateUser(req.params.id,req.body,function(err,rows){
        if(err){
            res.json(err);
        } else{
            res.json(rows);
        }
    });
});
module.exports=router;
