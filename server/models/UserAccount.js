var db = require('../dbconnect');

var UserAccount={
	getAllUser:function(callback){
		return db.query("Select * from useraccount",callback);
	},
	getUserById:function(id,callback){
		return db.query("select * from useraccount where Id=?",[id],callback);
	},
	addUser:function(useraccount,callback){
		return db.query("Insert into useraccount(email,password,roll) values(?,?,?)",[useraccount.email,useraccount.password,useraccount.roll],callback);
	},
	deleteUser:function(id,callback){
		return db.query("delete from useraccount where Id=?",[id],callback);
	},
	updateUser:function(id,useraccount,callback){
		return db.query("update useraccount set email=?,password=?,roll=? where Id=?",[useraccount.email,useraccount.password,useraccount.roll,id],callback);
	}
};
 module.exports=UserAccount;
